import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    products: [],
    detailproduct : {}
  },
  mutations: {
    SEARCH_PRODUCT(state, searechproducts) {
      state.products = searechproducts.map(item =>({
        id: item.id,
        title: item.title,
        description: item.description,
        price: item.price,
        vendor_name: item.vendor_name,
        vendor_support_email: item.vendor_support_email
      }));
    },
    DETAIL_PRODUCT(state, product) {
      state.detailproduct = product;
    },
    EMPTY_PRODUCT(state) {
      state.products = [];
    }
  },
  actions: {
    addProduct({ commit }, product) {
      axios
        .post("http://a1a6f6bb.ngrok.io/api/create.php", product)
        .then(response => response.data)
        .then(product => {
          
        })
        .catch(err => {
          console.log(err);
        });
    },
    searchProduct({commit}, query) {
      axios
        .get("http://a1a6f6bb.ngrok.io/api/search.php?query=" + query)
        .then(response => response.data)
        .then(products => {
          
          commit("SEARCH_PRODUCT", products);
        })
        .catch(err => {
          console.log(err);
          if(err.response.data.message === "Product does not exist.") {
            commit("EMPTY_PRODUCT");
          }
        });
    },
    detailProduct( { commit }, id) {
      axios
        .get("http://a1a6f6bb.ngrok.io/api/read.php?id=" + id)
        .then(response => response.data)
        .then(product => {
          commit("DETAIL_PRODUCT", product);
        })
        .catch(err => {
          console.log(err);
        });
    }
  },
  getters: {
    products: state => state.products
  }
});
